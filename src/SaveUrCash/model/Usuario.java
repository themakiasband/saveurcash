package SaveUrCash.model;

import SaveUrCash.bsn.UsuarioBsn;
import SaveUrCash.controller.PrincipalController;

import java.util.LinkedList;

public class Usuario {
    private String contrasena;
    private String nombre;
    private int salario = 0;
    private int saldo = 0;
    private UsuarioBsn usuarioBsn;
    //private LinkedList<Transaccion> ingresos;
    //private LinkedList<Transaccion> deudas;

    public Usuario(String passWord, String nombre, int salario) {
        this.contrasena = passWord;
        this.nombre = nombre;
        this.salario = salario;
        this.usuarioBsn = new UsuarioBsn();
    }

    public Usuario(String passWord, String nombre) {
        this.contrasena = passWord;
        this.nombre = nombre;
        this.usuarioBsn = new UsuarioBsn();
    }

    public Usuario() {
        this.usuarioBsn = new UsuarioBsn();
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public int getSaldo() {
        return saldo;
    }

    public void actualizarSaldo() {
        usuarioBsn.actualizarSaldo(PrincipalController.getUser());
    }

    public void actualizarDB(){
        usuarioBsn.actualizarDB();
        actualizarSalario();
        actualizarSaldo();
    }

    public void actualizarSalario(){
        usuarioBsn.actualizarSalario(PrincipalController.getUser());
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
}
