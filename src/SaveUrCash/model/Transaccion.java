package SaveUrCash.model;

import java.time.LocalDate;

public class Transaccion {
    private String descripcion;
    private int monto;
    private LocalDate fecha;
    private  String tipo;
    private String propietario;

    public Transaccion(String descripcion, int monto, LocalDate fecha, String tipo) {
        this.descripcion = descripcion;
        this.monto = monto;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public Transaccion(String descripcion, int monto) {
        this.descripcion = descripcion;
        this.monto = monto;
    }

    public Transaccion() {

    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
