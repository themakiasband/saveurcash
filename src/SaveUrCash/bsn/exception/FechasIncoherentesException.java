package SaveUrCash.bsn.exception;

public class FechasIncoherentesException extends Exception {

    public FechasIncoherentesException(){
        super("La fecha \"desde\" debe ser anterior a la fecha \"hasta\"");
    }

}
