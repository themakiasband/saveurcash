package SaveUrCash.bsn.exception;

public class UsuarioNoExisteException extends Throwable {

    public UsuarioNoExisteException() {
        super("No existe un usuario con el nombre ingresado. Regístrese.");
    }

}
