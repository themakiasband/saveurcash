package SaveUrCash.bsn.exception;

public class MaximoDeUsuariosException extends Exception {
    private static final int maxUsuarios = 3;

    public static int getMaxUsuarios() {
        return maxUsuarios;
    }

    public MaximoDeUsuariosException() {
        super("Se ha alcanzado el máximo número de usuarios: " + maxUsuarios);
    }
}
