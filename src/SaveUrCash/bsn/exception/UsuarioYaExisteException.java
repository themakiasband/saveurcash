package SaveUrCash.bsn.exception;

public class UsuarioYaExisteException extends Exception {
    public UsuarioYaExisteException() {
        super("Ya existe un usuario con el nombre ingresado");
    }

}
