package SaveUrCash.bsn.exception;

import SaveUrCash.dao.GastosDAONIO;

public class GastoMayorASaldoException extends Exception {

    public GastoMayorASaldoException(){
        super("No se puede ingresar un gasto mayor al saldo.");
    }
}
