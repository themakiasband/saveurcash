package SaveUrCash.bsn;

import SaveUrCash.dao.UsuarioDAONIO;
import SaveUrCash.model.Transaccion;
import SaveUrCash.model.Usuario;

import java.util.List;

public class UsuarioBsn {
    private UsuarioDAONIO usuarioDAO;

    public UsuarioBsn() {
        this.usuarioDAO = new UsuarioDAONIO();
    }

    public void actualizarSalario(Usuario user){
        usuarioDAO.actualizarSalario(user);
    }

    public void actualizarDB() {
        usuarioDAO.actualizarDB();
    }

    public void actualizarSaldo(Usuario user) {
        List<Transaccion> ingresos = usuarioDAO.getIngresos(user);
        List<Transaccion> gastos = usuarioDAO.getGastos(user);
        int nuevoSaldo = 0;
        for(Transaccion ingreso : ingresos){
            nuevoSaldo = nuevoSaldo + ingreso.getMonto();
        }
        for(Transaccion gasto : gastos){
            nuevoSaldo = nuevoSaldo - gasto.getMonto();
        }
        user.setSaldo(nuevoSaldo);
    }
}
