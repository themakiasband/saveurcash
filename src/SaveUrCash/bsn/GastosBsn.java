package SaveUrCash.bsn;

import SaveUrCash.bsn.exception.GastoMayorASaldoException;
import SaveUrCash.controller.PrincipalController;
import SaveUrCash.dao.GastosDAONIO;
import SaveUrCash.model.Transaccion;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GastosBsn {
    private GastosDAONIO gastosDAO;

    public GastosBsn(){
        this.gastosDAO = new GastosDAONIO();
    }

    public List<Transaccion> consultarGastos(LocalDate fechaDesde, LocalDate fechaHasta) {
        List<Transaccion> gastos = gastosDAO.consultarGastos();
        List<Transaccion> requeridos = new ArrayList<>();
        for(Transaccion gasto : gastos){
            if(gasto.getFecha().compareTo(fechaDesde)>=0
                    && gasto.getFecha().compareTo(fechaHasta)<=0){
                requeridos.add(gasto);
            }
        }
        return requeridos;
    }

    public void registrarGasto(Transaccion gasto) throws GastoMayorASaldoException {
        if(gasto.getMonto() > PrincipalController.getUser().getSaldo()) throw new GastoMayorASaldoException();
        this.gastosDAO.registrarGasto(gasto);
    }
}
