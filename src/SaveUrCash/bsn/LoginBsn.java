package SaveUrCash.bsn;

import SaveUrCash.bsn.exception.MaximoDeUsuariosException;
import SaveUrCash.bsn.exception.PassErrorException;
import SaveUrCash.bsn.exception.UsuarioNoExisteException;
import SaveUrCash.bsn.exception.UsuarioYaExisteException;
import SaveUrCash.dao.LoginDAONIO;
import SaveUrCash.model.Usuario;

import java.util.Optional;

public class LoginBsn {
    private LoginDAONIO loginDAO;

    public LoginBsn(){
        this.loginDAO = new LoginDAONIO();
    }


    public void registrarUsuario(Usuario nuevoUsuario) throws UsuarioYaExisteException, MaximoDeUsuariosException {
        Optional<Usuario> usuarioOptional = this.loginDAO.consultarPorNombre(nuevoUsuario.getNombre());
        if (usuarioOptional.isPresent()) {
            throw new UsuarioYaExisteException();
        } else {
            this.loginDAO.registrarUsuario(nuevoUsuario);
        }
    }

    public boolean validarUsuario(Usuario nuevoUsuario) throws UsuarioNoExisteException, PassErrorException {
        Optional<Usuario> usuarioOptional = this.loginDAO.consultarPorNombre(nuevoUsuario.getNombre());
        if (usuarioOptional.isEmpty()) {
            throw new UsuarioNoExisteException();
        } else {
            if(loginDAO.validarUsuario(nuevoUsuario)) return true;
            else throw new PassErrorException();
        }
    }
}
