package SaveUrCash.bsn;

import SaveUrCash.bsn.exception.FechasIncoherentesException;
import SaveUrCash.dao.IngresosDAONIO;
import SaveUrCash.dao.LoginDAONIO;
import SaveUrCash.model.Transaccion;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class IngresosBsn {
    private IngresosDAONIO ingresosDAO;

    public IngresosBsn(){
        this.ingresosDAO = new IngresosDAONIO();
    }

    public void registrarIngreso(Transaccion ingreso) {
        this.ingresosDAO.registrarIngreso(ingreso);
    }

    public List<Transaccion> consultarIngresos(LocalDate fechaDesde, LocalDate fechaHasta) {

        List<Transaccion> ingresos = ingresosDAO.consultarIngresos();
        List<Transaccion> requeridos = new ArrayList<>();
        for(Transaccion ingreso : ingresos){
            if(ingreso.getFecha().compareTo(fechaDesde)>=0
            && ingreso.getFecha().compareTo(fechaHasta)<=0){
                requeridos.add(ingreso);
            }
        }
        return requeridos;
    }
}
