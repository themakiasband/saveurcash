package SaveUrCash.util;

import javafx.scene.control.TextFormatter;

import java.text.NumberFormat;
import java.util.function.UnaryOperator;

public class FormatoMoneda implements UnaryOperator<TextFormatter.Change> {

    private final static int LONGITUD_MAXIMA_PERMITIDA = 11;

    @Override
    public TextFormatter.Change apply(TextFormatter.Change t) {
        if (!t.isContentChange())
            return null;
        if ("".equals(t.getControlNewText()))
            return t;
        if (t.getControlNewText().length() > LONGITUD_MAXIMA_PERMITIDA)
            return null;
        try {
            String formatInput = NumberFormat.getNumberInstance()
                    .format(NumberFormat.getNumberInstance().parse(t.getControlNewText()));
            t.setText(formatInput);
            t.setRange(0, t.getControlText().length());
            t.setAnchor(formatInput.length());
            t.setCaretPosition(formatInput.length());
            return t;
        } catch (Exception e) {
            // TODO logger.log(format exception)
            e.printStackTrace();
            return null;
        }
    }

    public static String quitarPuntos(String monto){
        String numeros = "0123456789";
        StringBuilder sinPuntos = new StringBuilder();
        for(int i=0; i<monto.length(); i++){
            if(numeros.indexOf(monto.charAt(i)) != -1){
                sinPuntos.append(monto.charAt(i));
            }
        }
        return sinPuntos.toString();
    }
}
