package SaveUrCash.dao;

import SaveUrCash.bsn.exception.MaximoDeUsuariosException;
import SaveUrCash.model.Usuario;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.nio.file.StandardOpenOption.APPEND;

public class LoginDAONIO {
    private final static String NOMBRE_ARCHIVO = "usuarios-Login";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final static Integer LONGITUD_REGISTRO = 43;
    private final static Integer LONGITUD_PASSWORD = 10;
    private final static Integer LONGITUD_NOMBRE = 20;
    private final static Integer LONGITUD_SALARIO = 13;


    public LoginDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void registrarUsuario(Usuario nuevoUsuario) throws MaximoDeUsuariosException {
        List<Usuario> registrados = consultarUsuarios();
        if(registrados.size() >= MaximoDeUsuariosException.getMaxUsuarios()) throw new MaximoDeUsuariosException();
        else {
            // Se obtiene la representación en String del estudiante
            String registro = parseUsuarioString(nuevoUsuario);
            // Se extraen los bytes del registro
            byte[] datosRegistro = registro.getBytes();
            // Se ponen los bytes en un buffer
            ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
            // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
            try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
                // se escriben los datos del buffer
                fc.write(byteBuffer);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
    private String parseUsuarioString(Usuario nuevoUsuario) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampo(nuevoUsuario.getContrasena(), LONGITUD_PASSWORD))
                .append(completarCampo(nuevoUsuario.getNombre(), LONGITUD_NOMBRE))
                .append(completarCampo(Integer.toString(nuevoUsuario.getSalario()), LONGITUD_SALARIO));
        return sb.toString();
    }

    private String completarCampo(String valor, Integer longitudCampo) {
        // si el valor del campo supera su longitud, se recorta hasta la longitud máxima permitida
        if (valor.length() > longitudCampo) {
            return valor.substring(0, longitudCampo);
        }
        // regex: Regular expresion
        return String.format("%1$" + longitudCampo + "s", valor);
    }

    public Optional<Usuario> consultarPorNombre(String nombre) {
        ArrayList<Usuario> usuarios = (ArrayList<Usuario>) consultarUsuarios();
        Optional<Usuario> userOptional;
        for(Usuario user : usuarios){
            userOptional = Optional.ofNullable(user);
            if(user.getNombre().equals(nombre)) return userOptional;
        }
        return Optional.empty();
    }

    public boolean validarUsuario(Usuario usuario){
        ArrayList<Usuario> usuarios = (ArrayList<Usuario>) consultarUsuarios();
        for(Usuario user : usuarios){
            if(user.getNombre().equals(usuario.getNombre()) && user.getContrasena().equals(usuario.getContrasena())) return true;
        }
        return false;
    }

    public List<Usuario> consultarUsuarios() {
        List<Usuario> usuarios = new ArrayList<>();
        // se abre un canal hacia el archivo para leer bytes
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            // se encarga de capturar 30 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            // lee paquetes de bytes hasta que llegue al final del archivo
            while (sbc.read(buffer) > 0) {
                // ubica el apuntador del buffer en la posición inicial
                buffer.rewind();
                // decodifica los bytes usando el juego de caracteres por defecto del sistema operativo
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Usuario user = parseBufferUsuario(registro);
                usuarios.add(user);
                //prepara al buffer para leer bytes del disco de nuevo
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return usuarios;
    }

    private Usuario parseBufferUsuario(CharBuffer registro) {
        Usuario user = new Usuario();

        String passWord = registro.subSequence(0, LONGITUD_PASSWORD).toString().trim();
        user.setContrasena(passWord);
        registro.position(LONGITUD_PASSWORD);
        registro = registro.slice();

        String nombre = registro.subSequence(0, LONGITUD_NOMBRE).toString().trim();
        user.setNombre(nombre);
        registro.position(LONGITUD_NOMBRE);
        registro = registro.slice();

        String salario = registro.subSequence(0, LONGITUD_SALARIO).toString().trim();
        int salarioConv = Integer.parseInt(salario);
        user.setSalario(salarioConv);
        return user;
    }

}
