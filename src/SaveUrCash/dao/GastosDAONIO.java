package SaveUrCash.dao;

import SaveUrCash.controller.PrincipalController;
import SaveUrCash.model.Transaccion;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;

public class GastosDAONIO {
    private String NOMBRE_ARCHIVO = PrincipalController.getUser().getNombre() + "-gastos";
    private Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static Integer LONGITUD_REGISTRO = 39;
    private final static Integer LONGITUD_MONTO = 9;
    private final static Integer LONGITUD_NOMBRE = 20;
    private final static Integer LONGITUD_FECHA = 10;

    public GastosDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void registrarGasto(Transaccion nuevoGasto)  {
        String registro = parseGastoString(nuevoGasto);
        byte[] datosRegistro = registro.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseGastoString(Transaccion nuevoGasto) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampo(nuevoGasto.getFecha().toString(), LONGITUD_FECHA))
                .append(completarCampo(nuevoGasto.getDescripcion(), LONGITUD_NOMBRE))
                .append(completarCampo(Integer.toString(nuevoGasto.getMonto()), LONGITUD_MONTO));
        return sb.toString();
    }

    private String completarCampo(String valor, Integer longitudCampo) {
        // si el valor del campo supera su longitud, se recorta hasta la longitud máxima permitida
        if (valor.length() > longitudCampo) {
            return valor.substring(0, longitudCampo);
        }
        // regex: Regular expresion
        return String.format("%1$" + longitudCampo + "s", valor);
    }

    public List<Transaccion> consultarGastos() {
        List<Transaccion> gastos = new ArrayList<>();
        // se abre un canal hacia el archivo para leer bytes
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            // se encarga de capturar 30 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            // lee paquetes de bytes hasta que llegue al final del archivo
            while (sbc.read(buffer) > 0) {
                // ubica el apuntador del buffer en la posición inicial
                buffer.rewind();
                // decodifica los bytes usando el juego de caracteres por defecto del sistema operativo
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Transaccion gasto = parseBufferGasto(registro);
                gastos.add(gasto);
                //prepara al buffer para leer bytes del disco de nuevo
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return gastos;
    }

    private Transaccion parseBufferGasto(CharBuffer registro) {
        Transaccion gasto = new Transaccion();

        String fecha = registro.subSequence(0, LONGITUD_FECHA).toString().trim();
        gasto.setFecha(LocalDate.parse(fecha));
        registro.position(LONGITUD_FECHA);
        registro = registro.slice();

        String nombre = registro.subSequence(0, LONGITUD_NOMBRE).toString().trim();
        gasto.setDescripcion(nombre);
        registro.position(LONGITUD_NOMBRE);
        registro = registro.slice();

        String monto = registro.toString().trim();
        int montoConv = Integer.parseInt(monto);
        gasto.setMonto(montoConv);
        gasto.setTipo("Gasto");
        return gasto;
    }
}
