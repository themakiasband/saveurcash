package SaveUrCash.dao;

import SaveUrCash.model.Transaccion;
import SaveUrCash.model.Usuario;

import java.util.List;
import java.util.Optional;

public class UsuarioDAONIO {
    private LoginDAONIO loginDAO;
    private IngresosDAONIO ingresosDAO;
    private GastosDAONIO gastosDAO;

    public UsuarioDAONIO(){
        this.loginDAO = new LoginDAONIO();
    }

    public void actualizarSalario(Usuario user){
        Optional<Usuario> userOptional = loginDAO.consultarPorNombre(user.getNombre());
        if(userOptional.isPresent()){
            Usuario userDB = userOptional.get();
            user.setSalario(userDB.getSalario());
        }else{
            System.out.println("F");
        }

    }
    public void actualizarDB(){
        this.gastosDAO = new GastosDAONIO();
        this.ingresosDAO = new IngresosDAONIO();
    }

    public List<Transaccion> getGastos(Usuario user) {
        List<Transaccion> gastos = gastosDAO.consultarGastos();
        return gastos;
    }

    public List<Transaccion> getIngresos(Usuario user) {
        List<Transaccion> ingresos = ingresosDAO.consultarIngresos();
        return ingresos;
    }
}
