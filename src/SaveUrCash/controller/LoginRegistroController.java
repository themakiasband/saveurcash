package SaveUrCash.controller;

import SaveUrCash.bsn.LoginBsn;
import SaveUrCash.bsn.exception.MaximoDeUsuariosException;
import SaveUrCash.bsn.exception.UsuarioYaExisteException;
import SaveUrCash.model.Usuario;
import SaveUrCash.util.FormatoMoneda;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginRegistroController {
    @FXML
    private PasswordField txtPass;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtSalario;

    private LoginBsn loginBsn;
    private static Stage stage;

    public static void setStage(Stage newStage) {
        stage = newStage;
    }

    public LoginRegistroController() {
        this.loginBsn = new LoginBsn();
    }

    @FXML
    public void initialize(){
        txtSalario.setTextFormatter(new TextFormatter<>(new FormatoMoneda()));
        txtNombre.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().length() <= 20) {
                return change;
            }
            return null;
        }));
        txtPass.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().length() <= 10) {
                return change;
            }
            return null;
        }));
    }

    public void btnRegistrar2_action(ActionEvent actionEvent) {

        String contraseñaIngresada = txtPass.getText().trim();
        String nombreIngresado = txtNombre.getText().trim();
        String salarioIngresado =txtSalario.getText().trim();
        System.out.println("Salario ingresado: " + salarioIngresado);
        int salarioConvertido;

        if (contraseñaIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de usuario", "Resultado de la operación", "La contraseña es requerido");
            txtPass.requestFocus();
            return;
        }

        if (nombreIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de usuario", "Resultado de la operación", "El nombre de usuario es requerido");
            txtNombre.requestFocus();
            return;
        }
        if(salarioIngresado.isEmpty()){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de usuario", "Resultado de la operación", "El salario de usuario es requerido");
            txtSalario.requestFocus();
            return;
        }

        try {
            System.out.println("Salario convertido: -" + FormatoMoneda.quitarPuntos(salarioIngresado)+"-");
            salarioConvertido = Integer.parseInt(FormatoMoneda.quitarPuntos(salarioIngresado));
            Usuario user = new Usuario(contraseñaIngresada, nombreIngresado, salarioConvertido);
            this.loginBsn.registrarUsuario(user);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de usuario", "Resultado de la operación", "El usuario ha sido registrado con éxito");
            limpiarCampos();
        } catch (UsuarioYaExisteException | MaximoDeUsuariosException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de usuario", "Resultado de la operación", e.getMessage());
        } catch (NumberFormatException e){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de usuario", "Resultado de la operación", "El salario debe ser un valor numérico");
        }
    }


    private void limpiarCampos() {
        this.txtPass.clear();
        this.txtNombre.clear();
        this.txtSalario.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }



    public void volverLogin_action() throws IOException {
        Stage login = new Stage();
        LoginController.setStage(login);
        login.setTitle("SaveUrCash");
        Parent ventanaLogin = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        login.setScene(new Scene(ventanaLogin, 300, 275));
        login.show();
        stage.close();
    }
}
