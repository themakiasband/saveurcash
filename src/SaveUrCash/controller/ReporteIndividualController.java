package SaveUrCash.controller;

import SaveUrCash.bsn.exception.FechasIncoherentesException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;

public class ReporteIndividualController {
    @FXML
    public RadioButton rbUno;
    @FXML
    public RadioButton rbTres;
    @FXML
    public RadioButton rbSeis;
    @FXML
    public RadioButton rbDoce;
    @FXML
    public DatePicker dpDesde;
    @FXML
    public DatePicker dpHasta;
    @FXML
    private ComboBox<String> cbOpcion;

    @FXML
    public void initialize() {
        cbOpcion.getItems().add("Ingresos");
        cbOpcion.getItems().add("Gastos");
        dpDesde.setValue(LocalDate.now());
        dpHasta.setValue(LocalDate.now());
    }

    public void btnGenerar_action() throws IOException {
        try {
            if (cbOpcion.getValue().equals("Ingresos")) MostrarIndividualController.setTipo('I');
            else if (cbOpcion.getValue().equals("Gastos")) MostrarIndividualController.setTipo('G');
            LocalDate[] fechas;
            if (rbUno.isSelected()) fechas = new LocalDate[]{LocalDate.now().minusMonths(1), LocalDate.now()};
            else if (rbTres.isSelected()) fechas = new LocalDate[]{LocalDate.now().minusMonths(3), LocalDate.now()};
            else if (rbSeis.isSelected()) fechas = new LocalDate[]{LocalDate.now().minusMonths(6), LocalDate.now()};
            else if (rbDoce.isSelected()) fechas = new LocalDate[]{LocalDate.now().minusMonths(12), LocalDate.now()};
            else if(dpDesde.getValue().compareTo(dpHasta.getValue())>0) throw new FechasIncoherentesException();
            else fechas = new LocalDate[]{dpDesde.getValue(),dpHasta.getValue()};
            MostrarIndividualController.setFechas(fechas);
            Stage individualR = new Stage();
            MostrarIndividualController.setStage(individualR);
            individualR.setTitle("SaveUrCash");
            Parent mostrarIndividual = FXMLLoader.load(getClass().getResource("../view/mostrarIndividual.fxml"));
            individualR.setScene(new Scene(mostrarIndividual));
            individualR.show();
            PrincipalController.getContainer().setDisable(true);
        } catch (NullPointerException e) {
            mostrarMensaje(Alert.AlertType.WARNING, "Generar Reportes", "Advertencia", "Seleccione el tipo de " +
                    "transacciones.");
        } catch (FechasIncoherentesException e){
            mostrarMensaje(Alert.AlertType.WARNING, "Generar Reportes", "Advertencia", e.getMessage());
        }
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    public void disableRB_action(ActionEvent actionEvent) {
        rbDoce.setSelected(false);
        rbSeis.setSelected(false);
        rbTres.setSelected(false);
        rbUno.setSelected(false);
    }
}
