package SaveUrCash.controller;

import SaveUrCash.bsn.LoginBsn;
import SaveUrCash.bsn.exception.PassErrorException;
import SaveUrCash.bsn.exception.UsuarioNoExisteException;
import SaveUrCash.bsn.exception.UsuarioYaExisteException;
import SaveUrCash.model.Usuario;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {
    @FXML
    private PasswordField txtPass;
    @FXML
    private TextField txtNombre;

    private LoginBsn loginBsn;
    private static Stage stage;

    @FXML
    public void initialize(){
        stage.setResizable(false);
        stage.setMaximized(false);
    }

    public static void setStage(Stage newStage){
        stage = newStage;
    }
    public LoginController(){
        this.loginBsn = new LoginBsn();
    }

    public void btnAbrirRegistro() throws IOException {
        Stage stage1=new Stage();
        Parent root=FXMLLoader.load(getClass().getResource(("../view/loginRegistro.fxml")));
        Scene scene=new Scene((root));
        stage1.setScene(scene);
        stage1.setTitle("Registrar");
        stage1.setResizable(false);
        stage1.show();
        LoginRegistroController.setStage(stage1);
        stage.close();

    }
    @FXML
    public void buttonPressed(KeyEvent e)
    {
        if(e.getCode().toString().equals("ENTER"))
        {
            btnEntrar_action();
        }
    }
    public void btnEntrar_action(){
        String contraseñaIngresada = txtPass.getText().trim();
        String nombreIngresado = txtNombre.getText().trim();
        Usuario user = new Usuario(contraseñaIngresada, nombreIngresado);
        try {
            this.loginBsn.validarUsuario(user);
            PrincipalController.setUser(user);
            Stage principal = new Stage();
            PrincipalController.setStage(principal);
            principal.setTitle("SaveUrCash");
            Parent ventanaPrincipal = FXMLLoader.load(getClass().getResource("../view/principal.fxml"));
            principal.setScene(new Scene(ventanaPrincipal));
            principal.show();
            stage.close();
        } catch (UsuarioNoExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Login", "Error al ingresar", e.getMessage());
        }catch (PassErrorException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Login", "Error al ingresar", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean validarCampos(String... campos) {
        boolean sonValidos = true;
        for (String campo : campos) {
            if (campo == null || campo.trim().isEmpty()) {
                sonValidos = false;
                break;
            }
        }
        return sonValidos;
    }

    private void limpiarCampos() {
        this.txtPass.clear();
        this.txtNombre.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
