package SaveUrCash.controller;

import SaveUrCash.model.Usuario;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;


public class PrincipalController {
    @FXML
    private  BorderPane contenedorPrincipal;
    @FXML
    private Label menuUser;
    private static Stage stage;
    private static Usuario user;
    private static BorderPane container;

    public static BorderPane getContainer() {
        return container;
    }

    @FXML
    public void initialize(){
        user.actualizarDB();
        menuUser.setText(user.getNombre());
        menuUser.setStyle("-fx-font-weight: bold;");
        stage.setResizable(false);
        stage.setMaximized(false);
        System.out.println("Usuario actual: " + user.getNombre());
        container = contenedorPrincipal;
    }

    public static void setUser(Usuario userL) {
        user = userL;
    }

    public static void setStage(Stage newStage) {
        stage = newStage;
    }

    public static Usuario getUser() {
        return user;
    }
    public static Stage getStage(){return stage;}

    public void mostrarSaldo_action() {
    }

    public void volverLogin_action() throws IOException {
        Stage login = new Stage();
        LoginController.setStage(login);
        login.setTitle("SaveUrCash");
        Parent ventanaLogin = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        login.setScene(new Scene(ventanaLogin));
        login.show();
        stage.close();
    }

    public void salir_action(){
        stage.close();
    }

    public void saldo_action() throws IOException {
        Parent mostrarSaldo = FXMLLoader.load(getClass().getResource("../view/mostrarSaldo.fxml"));
        contenedorPrincipal.setCenter(mostrarSaldo);
    }
    public void ingresos_action()throws IOException{
        Parent registrarIngresos = FXMLLoader.load(getClass().getResource("../view/registrarIngresos.fxml"));
        contenedorPrincipal.setCenter((registrarIngresos));
    }

    public void gastos_action()throws IOException{
        Parent registrarGastos = FXMLLoader.load(getClass().getResource("../view/registrarGastos.fxml"));
        contenedorPrincipal.setCenter((registrarGastos));
    }
    public void general_action()throws IOException{
        Parent reporteGeneral = FXMLLoader.load(getClass().getResource("../view/reporteGeneral.fxml"));
        contenedorPrincipal.setCenter((reporteGeneral));
    }
    public void individual_action()throws IOException{
        Parent reporteIndividual = FXMLLoader.load(getClass().getResource("../view/reporteIndividual.fxml"));
        contenedorPrincipal.setCenter((reporteIndividual));
    }


}
