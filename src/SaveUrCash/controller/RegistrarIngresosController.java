package SaveUrCash.controller;

import SaveUrCash.bsn.IngresosBsn;
import SaveUrCash.model.Transaccion;
import SaveUrCash.util.FormatoMoneda;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.time.LocalDate;

public class RegistrarIngresosController {

    private IngresosBsn ingresosBsn;
    @FXML
    private DatePicker dpFechaIngreso;
    @FXML
    private TextField txtMonto;
    @FXML
    private TextField txtDescripcion;

    @FXML
    public void initialize() {
        txtDescripcion.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().length() <= 20) {
                return change;
            }
            return null;
        }));
        txtMonto.setTextFormatter(new TextFormatter<>(new FormatoMoneda()));
        dpFechaIngreso.setValue(LocalDate.now());
        txtDescripcion.setText("Salario");
        txtMonto.setText(String.valueOf(PrincipalController.getUser().getSalario()));
    }

    public RegistrarIngresosController() {
        this.ingresosBsn = new IngresosBsn();
    }

    public void btnIngresos_action(ActionEvent actionEvent) {
        String descripcion = txtDescripcion.getText();
        String montoIngresado = txtMonto.getText();
        LocalDate fechaIngresada = dpFechaIngreso.getValue();
        if (montoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de ingreso", "Resultado de la operación", "El monto es requerido");
            txtMonto.requestFocus();
            return;
        }

        if (descripcion.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de ingreso", "Resultado de la operación", "La descripción es requerida");
            txtDescripcion.requestFocus();
            return;
        }
        if(LocalDate.now().compareTo(fechaIngresada)<0){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de ingreso", "Resultado de la operación", "La fecha no puede ser posterior a la actual");
            dpFechaIngreso.requestFocus();
            return;
        }
        int montoConvertido;
        montoConvertido = Integer.parseInt(FormatoMoneda.quitarPuntos(montoIngresado));
        Transaccion ingreso = new Transaccion(descripcion, montoConvertido, fechaIngresada, "Ingreso");
        this.ingresosBsn.registrarIngreso(ingreso);
        mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de ingreso", "Resultado de la operación", "El ingreso ha sido registrado con éxito");
        limpiarCampos();

    }

    private void limpiarCampos() {
        this.txtDescripcion.clear();
        this.txtMonto.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
