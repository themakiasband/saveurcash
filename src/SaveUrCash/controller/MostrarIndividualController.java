package SaveUrCash.controller;

import SaveUrCash.bsn.GastosBsn;
import SaveUrCash.bsn.IngresosBsn;
import SaveUrCash.model.Transaccion;
import SaveUrCash.util.FormatoMoneda;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MostrarIndividualController {
    @FXML
    private Label lblTipo;
    @FXML
    private Label lblFecha;
    @FXML
    private TableView<Transaccion> tblIndividual;
    @FXML
    private TableColumn<Transaccion, String> clmDescripcion;
    @FXML
    private TableColumn<Transaccion, String> clmFecha;
    @FXML
    private TableColumn<Transaccion, String> clmMonto;
    private GastosBsn gastosBsn;
    private IngresosBsn ingresosBsn;
    private static char tipo;
    private static LocalDate[] fechas;
    private static Stage stage;

    @FXML
    public void initialize(){
        stage.initStyle(StageStyle.UNDECORATED);
        clmDescripcion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDescripcion()));
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFecha().toString()));
        clmMonto.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getMonto())));
        refrescarTabla();
        //TODO clmMonto.setTextFormatter(new TextFormatter<>(new FormatoMoneda()));
    }

    public MostrarIndividualController(){
        this.ingresosBsn = new IngresosBsn();
        this.gastosBsn = new GastosBsn();
    }

    public void btnVolver_action() {
        PrincipalController.getContainer().setDisable(false);
        stage.close();
    }

    private void refrescarTabla(){
        List<Transaccion> transaccionList = new ArrayList<>();
        if(tipo == 'I'){
            transaccionList = ingresosBsn.consultarIngresos(fechas[0],fechas[1]);
            lblTipo.setText("Se muestran transacciones de tipo: Ingresos");
        }
        if(tipo == 'G'){
            transaccionList = gastosBsn.consultarGastos(fechas[0],fechas[1]);
            lblTipo.setText("Se muestran transacciones de tipo: Gastos");
        }
        lblFecha.setText("Se muestran transacciones desde " + fechas[0] +" hasta "+fechas[1]);
        ObservableList<Transaccion> estudiantesListObservable = FXCollections.observableList(transaccionList);
        tblIndividual.setItems(estudiantesListObservable);
    }

    public static LocalDate[] getFechas() {
        return fechas;
    }

    public static void setFechas(LocalDate[] fechas) {
        MostrarIndividualController.fechas = fechas;
    }

    public static void setStage(Stage stage) {
        MostrarIndividualController.stage = stage;
    }

    public static void setTipo(char tipo) {
        MostrarIndividualController.tipo = tipo;
    }

}
