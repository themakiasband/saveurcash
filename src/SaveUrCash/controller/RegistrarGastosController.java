package SaveUrCash.controller;
import SaveUrCash.bsn.GastosBsn;
import SaveUrCash.bsn.exception.GastoMayorASaldoException;
import SaveUrCash.model.Transaccion;
import SaveUrCash.util.FormatoMoneda;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.time.LocalDate;

public class RegistrarGastosController {

    private GastosBsn gastosBsn;

    @FXML
    private TextField txtDescripcion;
    @FXML
    private TextField txtMonto;
    @FXML
    private DatePicker dpFecha;
    @FXML
    private Label lblSaldo;
    @FXML
    public void initialize(){
        txtDescripcion.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().length() <= 20) {
                return change;
            }
            return null;
        }));
        txtMonto.setTextFormatter(new TextFormatter<>(new FormatoMoneda()));
        dpFecha.setValue(LocalDate.now());
        lblSaldo.setText("Saldo Disponible: "+ PrincipalController.getUser().getSaldo());
    }

    public RegistrarGastosController(){
        this.gastosBsn = new GastosBsn();
    }
    public void btnGastos_action() {
        String descripcion = txtDescripcion.getText();
        String montoIngresado = txtMonto.getText();
        LocalDate fechaIngresada = dpFecha.getValue();
        if (montoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gasto", "Resultado de la operación", "El monto es requerido");
            txtMonto.requestFocus();
            return;
        }

        if (descripcion.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gasto", "Resultado de la operación", "La descripción es requerida");
            txtDescripcion.requestFocus();
            return;
        }
        if(LocalDate.now().compareTo(fechaIngresada)<0){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de ingreso", "Resultado de la operación", "La fecha no puede ser posterior a la actual");
            dpFecha.requestFocus();
            return;
        }
        int montoConvertido;

        try{
            montoConvertido = Integer.parseInt(FormatoMoneda.quitarPuntos(montoIngresado));
            Transaccion gasto = new Transaccion(descripcion, montoConvertido, fechaIngresada, "Gasto");
            this.gastosBsn.registrarGasto(gasto);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de gasto", "Resultado de la operación", "El gasto ha sido registrado con éxito");
            limpiarCampos();
            PrincipalController.getUser().actualizarSaldo();
            lblSaldo.setText("Saldo Disponible: "+ PrincipalController.getUser().getSaldo());
        }catch (GastoMayorASaldoException e){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gasto", "Resultado de la operación", e.getMessage());
        }
    }

    private void limpiarCampos() {
        this.txtDescripcion.clear();
        this.txtMonto.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
