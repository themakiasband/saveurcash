package SaveUrCash.controller;

import SaveUrCash.util.FormatoMoneda;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;

public class MostarSaldoController {

    @FXML
    private TextField txtUsuario;
    @FXML
    private TextField txtSaldo;
    @FXML
    public void initialize(){
        txtSaldo.setTextFormatter(new TextFormatter<>(new FormatoMoneda()));
        txtUsuario.setText(PrincipalController.getUser().getNombre());
        PrincipalController.getUser().actualizarSaldo();
        txtSaldo.setText(String.valueOf(PrincipalController.getUser().getSaldo()));
    }

}
