package SaveUrCash.controller;

import SaveUrCash.bsn.GastosBsn;
import SaveUrCash.bsn.IngresosBsn;
import SaveUrCash.model.Transaccion;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MostrarGeneralController {

    @FXML
    private Label lblFecha;
    @FXML
    private TableView<Transaccion> tblIndividual;
    @FXML
    private TableColumn<Transaccion, String> clmDescripcion;
    @FXML
    private TableColumn<Transaccion, String> clmFecha;
    @FXML
    private TableColumn<Transaccion, String> clmMonto;
    @FXML
    private TableColumn<Transaccion, String> clmTipo;
    private GastosBsn gastosBsn;
    private IngresosBsn ingresosBsn;
    private static LocalDate[] fechas;
    private static Stage stage;

    @FXML
    public void initialize(){
        stage.initStyle(StageStyle.UNDECORATED);
        clmDescripcion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDescripcion()));
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFecha().toString()));
        clmMonto.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getMonto())));
        clmTipo.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getTipo())));
        refrescarTabla();
        //TODO clmMonto.setTextFormatter(new TextFormatter<>(new FormatoMoneda()));
    }

    public MostrarGeneralController(){
        this.ingresosBsn = new IngresosBsn();
        this.gastosBsn = new GastosBsn();
    }
    public static void setFechas(LocalDate[] fechas) {
        MostrarGeneralController.fechas = fechas;
    }

    public void btnVolver_action() {
        PrincipalController.getContainer().setDisable(false);
        stage.close();
    }

    public static void setStage(Stage stage) {
        MostrarGeneralController.stage = stage;
    }

    private void refrescarTabla(){
        List<Transaccion> transaccionList = new ArrayList<>();
        transaccionList.addAll(ingresosBsn.consultarIngresos(fechas[0],fechas[1]));
        transaccionList.addAll(gastosBsn.consultarGastos(fechas[0],fechas[1]));
        lblFecha.setText("Se muestran transacciones desde " + fechas[0] +" hasta "+fechas[1]);
        ObservableList<Transaccion> estudiantesListObservable = FXCollections.observableList(transaccionList);
        tblIndividual.setItems(estudiantesListObservable);
    }
}
