package SaveUrCash;

import SaveUrCash.controller.LoginController;
import SaveUrCash.controller.PrincipalController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        /*Parent root = FXMLLoader.load(getClass().getResource("view/login.fxml"));
        primaryStage.setTitle("SaveUrCash");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();*/
        LoginController.setStage(primaryStage);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/login.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("SaveUrCa$h");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
